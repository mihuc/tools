layui.use(['element', 'form'], function () {
    var element = layui.element,
        form = layui.form;
    var navs = [
        {
            title: '在线工具',
            children: [
                {
                    title: '摩斯密码',
                    tag: '加密/解密',
                    describe: '摩斯密码',
                    src: '/tools/modules/xmorse'
                },
                {
                    title: '数字转大写',
                    tag: '金额/汉字',
                    describe: '数字转大写',
                    src: '/tools/modules/daxie'
                },
                {
                    title: '文字加密解密',
                    tag: '加密/解密',
                    describe: 'Base64、Url、MD5',
                    src: '/tools/modules/encdec'
                },
                {
                    title: '流量消耗器',
                    tag: '工具',
                    describe: '多线程消耗流量',
                    src: '/tools/modules/flow_consumer'
                }
            ]
        }
    ]
    layui.toolsnavs = navs;
    $(".tools-nav").html('');
    for (var i = 0; i < navs.length; i++) {
        var aa = '<li class="layui-nav-item"><a href="' + (navs[i].src ? navs[i].src : 'javascript:;') + '">' + navs[i].title + '</a>';
        if (navs[i].children) {
            aa += '<dl class="layui-nav-child">';
            for (var j = 0; j < navs[i].children.length; j++) {
                aa += '<dd><a href="' + (navs[i].children[j].src ? navs[i].children[j].src : 'javascript:;') + '">' + navs[i].children[j].title + '</a></dd> ';
            }
            aa += '</dl>';
        }
        aa += '</li>'
        $(".tools-nav").append(aa);
    }
    element.render('nav', 'tools-nav');
    $('.fly-logo').html('<img src="/tools/static/images/logo.png" alt="迷糊虫的工具箱">迷糊虫的工具箱')
    $('body').append([
        '<div class="fly-footer layui-col-sm12 layui-col-md12">     ',
        '    <div>                                                  ',
        '        <span>友情链接</span>                                ',
        '        <span><a href="//www.baidu.com" target="_blank">百度</a></span>     ',
        '        <span><a href="//gitee.com/mihuc" target="_blank">码云</a></span>        ',
        '        <span><a href="//github.com/kingxjs" target="_blank">GitHub</a></span>    ',
        '        <span><a href="//tool.lu/" target="_blank">在线工具</a></span>      ',
        '    </div>                                                 ',
        '    <p>                                                    ',
        '        © 2020 <a href="/tools">mihuc</a>                  ',
        '    </p>                                                   ',
        '</div>                                                     ',
    ].join(''))
})
if (isWeiXin()) {
    $('.fly-header').hide();
}
function isWeiXin() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == "micromessenger" || ua.match(/WeiBo/i) == "weibo") {
        return true;
    } else if (ua.indexOf('mobile mqqbrowser') > -1) {
        return true;
    } else if (ua.indexOf('iphone') > -1 || ua.indexOf('mac') > -1) {
        if (ua.indexOf('qq') > -1) {
            return true;
        }
    }
    return false;
}

(function () {
    var canonicalURL, curProtocol;
    //Get the <link> tag
    var x = document.getElementsByTagName("link");
    //Find the last canonical URL
    if (x.length > 0) {
        for (i = 0; i < x.length; i++) {
            if (x[i].rel.toLowerCase() == 'canonical' && x[i].href) {
                canonicalURL = x[i].href;
            }
        }
    }
    //Get protocol
    if (!canonicalURL) {
        curProtocol = window.location.protocol.split(':')[0];
    }
    else {
        curProtocol = canonicalURL.split(':')[0];
    }
    //Get current URL if the canonical URL does not exist
    if (!canonicalURL) canonicalURL = window.location.href;
    //Assign script content. Replace current URL with the canonical URL
    !function () { var e = /([http|https]:\/\/[a-zA-Z0-9\_\.]+\.baidu\.com)/gi, r = canonicalURL, t = document.referrer; if (!e.test(r)) { var n = (String(curProtocol).toLowerCase() === 'https') ? "https://sp0.baidu.com/9_Q4simg2RQJ8t7jm9iCKT-xh_/s.gif" : "//api.share.baidu.com/s.gif"; t ? (n += "?r=" + encodeURIComponent(document.referrer), r && (n += "&l=" + r)) : r && (n += "?l=" + r); var i = new Image; i.src = n } }(window);
})();